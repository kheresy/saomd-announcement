# SAOMD-Announcement

這是[《刀劍神域 記憶重組》（Sword Art Online - Memory Defrag）](https://sao-md.bn-ent.net/as/)的公告備份、分析擷取器。

主要程式是透過 [Node.js](https://nodejs.org/) 來撰寫的腳本，透過 GitLab CI/CD 提供的 shared runner 固定執行。

處理過的資料，會放到 [GitHub 的專案](https://github.com/KHeresy/SAOMD-AA)，並可以用 GitHub Page 的形式來瀏覽。網頁是：


>  https://kheresy.github.io/SAOMD-AA/


其執行流程大致如下：

1. 取得舊資料
    1. 透過 Git 由 GitHub 擷取之前的資料
2. 更新資料
    1. 使用 Node.js 去讀取官方公告網頁
    2. 檢查索引頁的變更
    3. 檢查各公告頁面是否有變化
3. 如果有發現有公告變化的話，透過 Git 更新到 GitHub 