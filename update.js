﻿'use strict';

var output_path = "./content/";
var ref_path = "./SAOMD-AA/";
var logFile = "list.json";

// get current time
var dateFormat = require('dateformat');
var now = dateFormat(new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Taipei' })), "yyyy-mm-dd HH:MM:ss" );

function decodeUnicode(iString) {
	return iString.replace(/&#(\w+);/gi, function (match, numStr) {
		return String.fromCharCode('0' + numStr);
	});
}

function downloadFile(url, filename) {
	let fetch = require('node-fetch');
	fetch(url).then(res => res.buffer()).then(content => {
		let fs = require('fs');
		fs.writeFile(filename, content, (err) => {
			if (err) {
				throw err;
			}
		});
	});
}

function removeTagVersion(obj, tag, attr) {
	let $ = obj;
	$(tag).each(function (index) {
		let link = $(this).attr(attr);
		if (typeof link != "undefined") {
			link = link.replace(/\?v=(\d+)/gi, function (match, numStr) { return ""; });
			$(this).attr(attr, link);
		}
	});
}

function removeVersion(obj) {
	removeTagVersion(obj, "img", "src");
	removeTagVersion(obj, "link", "href");
	removeTagVersion(obj, "script", "src");

	var $ = obj;
	obj('script').each(function (index) {
		var js = $(this).contents().text();
		if (js.length > 0) {
			js = js.replace(/\?v=(\d+)/gi, function (match, numStr) { return ""; });
			$(this).text(js);
		}
	});
}

class CAItemNode {
	constructor(obj) {
		this.node = obj;
		this.adate = obj.children("dt").text();
		this.cdate = now;
		this.udate = now;
		this.status = "new";
		this.title = obj.children("dd").children("h2, div").text();

		// get absolute uri
		let link = obj.attr("onclick");
		let startpos = link.indexOf("id=") + 3;
		this.id = link.substring(startpos, link.indexOf("&", startpos));
	}

	getLink() {
		return "https://api-defrag-ap.wrightflyer.net/webview/announcement-detail?id=" + this.id + "&phone_type=2&lang=tc";
	}

	checkAndUpdate(path) {
		let filename = path + "/" + this.id + ".html";
		this.node.attr("onclick", "javascript:location.href='" + filename + "'");

		let fetch = require('node-fetch');
		return fetch(this.getLink()).then(res => res.text()).then(content => {
			const cheerio = require('cheerio');
			const $ = cheerio.load(content);
			removeVersion($);

			// modify back link
			$('.btnPageBack').attr('href', "../main.html");

			//TODO: download images?
			let fsW = require('fs');
			fsW.writeFile(output_path + filename, decodeUnicode($.html()), (err) => { });

			let fsR = require('fs');
			if (fsR.existsSync(ref_path + filename)) {
				const c2 = require('cheerio');
				const ref = c2.load(fsR.readFileSync(ref_path + filename));

				let a = ref('body').html().replace(/&#xA;/g, "\n");
				let b = $('body').html().replace(/&#xA;/g, "\n");
				if (a != b) {
					console.log(" > compare detail for " + this.id);
					
					// make detail comparsion
					let eA = ref('dd').children();
					let eB = $('dd').children();
					let bChanged = false;
					if (eA.length == eB.length) {
						for (let i = 0; i < eA.length; ++i) {
							let ieA = require("cheerio").default(eA[i]).wrap('<p/>');
							let ieB = require("cheerio").default(eB[i]).wrap('<p/>');
							if (ieA.parent().html() != ieB.parent().html()) {
								let ce = ieA[0];
								if (ce.type == 'tag') {
									if (ce.name == 'input' && ce.attribs['type'] == 'hidden' && ce.attribs['class'] == 'translate_object_list') {
										// character changed detected
										console.log("  - character changed");
										// skip next node, which is <li>, include character info
										++i;
									}
									else {
										bChanged = true;
										break;
									}
								}
								else {
									console.log('bbb');
									bChanged = true;
									break;
								}
							}
						}
					}

					if (bChanged)
						this.status = "updated";
					else
						this.status = "updated_conly";
				}
				else {
					this.status = "exist";
				}
			}
		});
	}

	json() {
		return {
			id: this.id,
			title: this.title,
			adate: this.adate,
			cdate: this.cdate,
			udate: this.udate,
			status: this.status,
			link: this.link
		};
	}
}

class SAOMD_AA {
	constructor(linkMain, pathRef, pathOutput) {
		// input path
		this.link = linkMain;
		this.refBase = pathRef;
		this.pathOut = pathOutput;

		// pre-defined filename
		this.listFile = "list.json";
		this.removedFile = "removed.json";
		this.changelogFile = "changelog.json";
		this.archiveFile = "archive.json";
		this.archivePath = "./archive/"

		this.ResultList = [];
		this.RemovedList = [];
		this.ChangedList = [];
	}

	start() {
		// load reference list
		console.log("Download reference base");
		let refJson = [];
		{
			let fsRef = require('fs');
			let refFile = this.refBase + this.listFile;
			if (fsRef.existsSync(refFile)) {
				refJson = require(refFile);
			}
		}

		// load web page
		console.log("Download main page");

		let fetch = require('node-fetch');
		fetch(this.link).then(res => res.text()).then(content => {
			const cheerio = require('cheerio');
			const $ = cheerio.load(content);
			removeVersion($);

			// get items
			let list = $('dl');
			console.log(" > Found " + list.length + " elements");

			let newData = [];
			let jobs = [];
			// process each item
			list.each(function (index) {
				var item = new CAItemNode($(this));
				console.log(item.id + "<" + item.adate + ">" + item.title);

				newData.push(item);
				jobs.push(item.checkAndUpdate("data"));
			});

			// sort by id
			newData.sort((a, b) => {
				return a.id - b.id;
			});

			console.log(" > Wait all page finsish processing...");
			Promise.all(jobs).then(v => {
				// convert to json data
				let jsonData = [];
				newData.forEach((v) => { jsonData.push(v.json()); });

				// compare with reference data
				console.log( " > Compare " + jsonData.length + " with " + refJson.length + " data");
				this.compareWithRef(jsonData, refJson);

				if (this.ChangedList.length > 0) {
					console.log(" > " + this.ChangedList.length + " data changed!");

					this.ChangedList.forEach((v) => {
						console.log(v.id + " - " + v.status + ": " + v.title);
					});

					this.writeMainHtml($);
					this.writeReferenceList();
					this.writeChangedList();
					this.writeRemovedList();

					// write dummy file for next job to commit
					let fs = require('fs');
					fs.writeFile("NEED_COMMIT", now, (err) => { if (err) { throw err; } });
				}
				else {
					console.log(" > No data changed");
				}
			});
		});
	}

	setNew(newItem) {
		newItem.status = "new";
		this.ChangedList.push(newItem);
		this.ResultList.push(newItem);
	}

	setRemoved(refItem) {
		if (refItem.status == "removed") {
			this.RemovedList.push(refItem);
		}
		else {
			refItem.udate = now;
			refItem.status = "removed";
			this.ChangedList.push(refItem);
			this.ResultList.push(refItem);
		}
	}

	setUpdated(newItem, refItem) {
		newItem.cdate = refItem.cdate;
		newItem.udate = now;
		newItem.status = "updated";
		this.ChangedList.push(newItem);
		this.ResultList.push(newItem);
	}

	equal(n1, n2) {
		return (n1.id == n2.id) && (n1.title == n2.title) && (n1.adate == n2.adate);
	}

	compareWithRef(newData, refData) {
		let idxNew = 0;
		let idxRef = 0;
		while ((idxNew < newData.length) && (idxRef < refData.length)) {
			let newItem = newData[idxNew];
			let refItem = refData[idxRef];

			if (newItem.id == refItem.id) {
				if (this.equal(newItem, refItem)) {
					if (newItem.status == "updated") {
						this.setUpdated(newItem, refItem);
					}
					else {
						refItem.status = "exist";
						this.ResultList.push(refItem);
					}
				}
				else {
					this.setUpdated(newItem, refItem);
				}
				++idxNew;
				++idxRef;
			} else if ((newItem.id - refItem.id) < 0) {
				this.setNew(newItem);
				++idxNew;
			} else {
				this.setRemoved(refItem);
				++idxRef;
			}
		};

		if (idxNew == newData.length) {
			while (idxRef < refData.length) {
				this.setRemoved(refData[idxRef]);
				++idxRef;
			}
		}
		if (idxRef == refData.length) {
			while (idxNew < newData.length) {
				this.setNew(newData[idxNew]);
				++idxNew;
			}
		}
	}

	writeMainHtml(htmlNode) {
		let $ = htmlNode;

		// process script files of main page
		$('script').each(function () {
			let link = $(this).attr("src");
			if (typeof link != "undefined") {
				if (link.startsWith("https://assets-defrag-gl.gree.net/")) {
					var filename = link.substring(link.lastIndexOf("/") + 1);
					downloadFile(link, this.pathOut + filename);
					$(this).attr("src", filename);
				}
			}
		});

		// write HTML file
		let fsHtml = require('fs');
		fsHtml.writeFile(this.pathOut + "main.html", decodeUnicode($.html()), (err) => {
			if (err) {
				throw err;
			}
			console.log("The HTML file was saved");
		});
	}

	writeReferenceList() {
		let fsJson = require('fs');
		fsJson.writeFile(this.pathOut + this.listFile, JSON.stringify(this.ResultList, null, 2), (err) => {
			if (err) { throw err; }
			console.log(" > The refernece file was saved");
		});
	}

	writeChangedList() {
		let refJson = [];
		{
			let fsRef = require('fs');
			let refFile = this.refBase + this.changelogFile;
			if (fsRef.existsSync(refFile)) {
				refJson = require(refFile);
			}
		}

		//archive old log
		if (refJson.length > 0) {
			let monthNow = new Date().getMonth();
			let monthArchive = new Date(refJson[0].date).getMonth();
			console.log(" > Current month: " + (monthNow + 1) + ", last lof month: " + (monthArchive + 1));

			let monthDef = monthNow - monthArchive;
			if (monthDef > 1 || (monthDef < 0 && monthDef + 12 > 1)) {
				let archiveJson = [];
				let keepJson = [];

				let bArchive = true;
				for (let idx = 0; idx < refJson.length; ++idx) {
					if (monthArchive != new Date(refJson[idx].date).getMonth())
						bArchive = false;

					if (bArchive) {
						archiveJson.push(refJson[idx]);
					} else {
						keepJson.push(refJson[idx]);
					}
				}

				if (archiveJson.length > 0) {
					console.log("> Archive " + archiveJson.length + " old log");

					let filename = dateFormat(new Date(refJson[0].date),"yyyy-mm") + ".json";

					let fsArchiveLog = require('fs');
					fsArchiveLog.writeFile(this.pathOut + this.archivePath + filename, JSON.stringify(archiveJson, null, 2), (err) => {
						if (err) { throw err; }
						console.log(" > The archive file " + filename + " was saved");
					});

					let archiveList = { "list": [] };
					let listFile = this.refBase + this.archiveFile;
					let fsArchiveList = require('fs');
					if (fsArchiveList.existsSync(listFile)) {
						archiveList = require(listFile);
						console.log(" > Load " + archiveList.list.length + " old archive list");
					}

					archiveList.list.push(filename);
					fsArchiveList.writeFile(this.pathOut + this.archiveFile, JSON.stringify(archiveList, null, 2), (err) => {
						if (err) { throw err; }
						console.log(" > The archive list file was saved");
					});
				}

				refJson = keepJson;
			}
		}

		this.ChangedList.sort(function (a, b) {
			if (a.status == b.status)
				return 0;

			if (a.status == "new" || b.status == "removed")
				return -1;
			if (a.status == "removed" || b.status == "new")
				return 1;

			return 0;
		});

		refJson.push({
			"date": now,
			"items": this.ChangedList
		});

		let fsJson = require('fs');
		fsJson.writeFile(this.pathOut + this.changelogFile, JSON.stringify(refJson, null, 2), (err) => {
			if (err) { throw err; }
			console.log(" > The changed file was saved");
		});
	}

	writeRemovedList() {
		if (this.RemovedList.length > 0) {
			//load first and update
			let refJson = [];
			{
				let fsRef = require('fs');
				let refFile = this.refBase + this.removedFile;
				if (fsRef.existsSync(refFile)) {
					refJson = require(refFile);
				}
			}

			// append to old
			refJson.push(...this.RemovedList);

			// write to file
			let fsJson = require('fs');
			fsJson.writeFile(this.pathOut + this.removedFile, JSON.stringify(refJson, null, 2), (err) => {
				if (err) { throw err; }
				console.log(" > The removed file was saved");
			});
		}
	}
}

new SAOMD_AA("https://api-defrag-ap.wrightflyer.net/webview/announcement?phone_type=2&lang=tc", "./SAOMD-AA/", "./content/").start();

